(function ($) {
	'use strict';

	$.fn.hasAttr = function(t) {
        var e = this;
        return void 0 !== e.attr(t) ? !0 : !1
    };

    function toogleFullPage(vars) {
		$.fn.fullpage.setAllowScrolling(vars);
	}

	var TeslaThemes = {

		init: function () {
			this.onePageSettings();
			this.smallToggles();
			this.slickInit();
			this.instagramFeed();
			this.isotopeInit();
		},

		onePageSettings: function () {
			$('#scroll-content').fullpage({
				scrollingSpeed: 1000,
				easing: 'easeInOutCubic',
				navigation: true,
				navigationPosition: 'left',
				keyboardScrolling: false,
			});

			var section = $('.slimScrollable');
			var cPos  = 0;

			section.each(function(){
				$(this).slimScroll({
				    height: 'auto',
				    size: '10px',
				    color: '#000000',
				    distance: '1px',
				    touchScrollStep: '50'
				}).bind('slimscrolling', function(e, pos){
					console.log(pos);

				});

				$(this).mouseenter(function() {
	        		if($(this).find('.inner').height() > $(window).height()) {
				    	$.fn.fullpage.setAllowScrolling(false);
	        		}
	        		else {
	        			$.fn.fullpage.setAllowScrolling(true);
	        		}
				}).mouseleave(function(){
				   $.fn.fullpage.setAllowScrolling(true);
				});
			});
		},

		smallToggles: function () {
			var ttPopup = $('.tt-popup');
			var blogPostPopup = $('.blog-post-popup');
			var teamPopup = $('.team-popup');

			ttPopup.each(function(){
				$(this).find('.close').on('click', function(){
					ttPopup.removeClass('open');
				});
			});

			$('.menu-trigger').on('click', function(){
				$('header').addClass('open');
			});

			$('header').find('.close').on('click', function(){
				$('header').removeClass('open');
			});

			$('.team-members .team-member').on('click', function(){
				teamPopup.addClass('open');
			});

			$('.blog-post .popup-readmore').on('click', function(){
				blogPostPopup.addClass('open');
			});

			$('.error-button').on('click', function(e){
				e.preventDefault();

				$.fn.fullpage.moveTo(1);
			});

			function initialize() {
				var mapCanvas = document.getElementsByClassName('map');
		        var maps = [];

		        jQuery(mapCanvas).each(function(i){

		            var lati = jQuery(this).data('lat');
		            var longi = jQuery(this).data('long');
		            var icon = jQuery(this).data('icon');

		            var mapOptions = {
		                center: new google.maps.LatLng(lati, longi),
		                zoom: 12,
		                styles: [{ stylers: [{saturation: -100}]}],
		                scrollwheel: false,
		                mapTypeId: google.maps.MapTypeId.ROADMAP
		            };
		            maps[i] = new google.maps.Map(mapCanvas[i], mapOptions);

		            var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lati, longi),
                        map: maps[i],
                        icon: icon,
                    });
		        });
		    }

            if(jQuery('.map').length) {
		        google.maps.event.addDomListener(window, 'load', initialize);
		    }
		},

		slickInit: function () {
			var i = $(".slick-carousel");

            i.each(function() {
                var e = $(this),
                    a = e.find(".carousel-items");
                a.slick({
                    focusOnSelect: !0,
                    speed: e.hasAttr("data-speed") ? e.data("speed") : 600,
                    slidesToShow: e.hasAttr("data-items-desktop") ? e.data("items-desktop") : 4,
                    arrows: e.hasAttr("data-arrows") ? e.data("arrows") : !0,
                    appendArrows: e,
                    dots: e.hasAttr("data-dots") ? e.data("dots") : !0,
                    infinite: e.hasAttr("data-infinite") ? e.data("infinite") : !1,
                    slidesToScroll: e.hasAttr("data-items-to-slide") ? e.data("items-to-slide") : 1,
                    initialSlide: e.hasAttr("data-start") ? e.data("start") : 0,
                    asNavFor: e.hasAttr("data-as-nav-for") ? e.data("as-nav-for") : "",
                    centerMode: e.hasAttr("data-center-mode") ? e.data("center-mode") : "",
                    fade: e.hasAttr("data-fade") ? e.data("fade") : false,
                    easing: e.hasAttr("data-easing") ? e.data("easing") : "linear",
                })
            })
		},

		instagramFeed: function () {
		    function themeInstagram(container, data) {
				var pattern, renderTemplate, url;
				url = 'https://api.instagram.com/v1';
				pattern = function(obj) {
					var item, k, len, template;
					if (obj.length) {
						template = '';
						for (k = 0, len = obj.length; k < len; k++) {
							item = obj[k];
							template += "<li><a href='" + item.link + "' title='" + item.title + "' target='_blank'><img src='" + item.image + "' alt='" + item.title + "'></a></li>";
						}
						return container.append(template);
					}
				};

				if (container.data('instagram-username')) {
					url += "/users/search?q=" + (container.data('instagram-username')) + "&client_id=" + data.clientID + "&callback=?";
					renderTemplate = this._template;

					$.ajax({
						dataType: "jsonp",
						url: url,
						data: data,
						success: function(response) {
						  	var urlUser;
						  	if (response.data.length) {
						    	urlUser = "https://api.instagram.com/v1/users/" + response.data[0].id + "/media/recent/?client_id=" + data.clientID + "&count=" + data.count + "&size=l&callback=?";
						    	return $.ajax({
						      		dataType: "jsonp",
						      		url: urlUser,
						      		data: data,
						      		success: function(response) {
						        		var instagramFeed;
						        		if (response.data.length) {
						          			instagramFeed = {};
						          			instagramFeed.data = renderTemplate(response);
						          			return pattern(instagramFeed.data);
						        		}
						      		}
						    	});
						  	}
						}
					});
				}
		    }

		    themeInstagram.prototype._template = function(obj) {
			  	var item, k, len, ref, results;
			  	if (obj.data) {
			    	ref = obj.data;
			    	results = [];
			    	for (k = 0, len = ref.length; k < len; k++) {
			      		item = ref[k];
			      		results.push({
			        	title: item.user.username,
			        	link: item.link,
			        	image: item.images.low_resolution.url
			     		});
			    	}
			    	return results;
			  	}
		    };

		  	if ($('[data-instagram]').length) {
		    	var iContainer = $('[data-instagram]');
		    	iContainer.each(function(){
		    		var whiteInstagram = new themeInstagram($(this), {
			      		clientID: '632fb01c8c0d43d7b63da809d0b6a662',
			      		count: $(this).data('instagram') || 6
			    	});
		    	});
		  	}
		},

		isotopeInit: function () {
			var sidebarWrapper = $('.sidebar .isotope-items'),
				isotopeContainer = $('.isotope-container'),
				defaultSelection = isotopeContainer.attr('data-default-selection');

			isotopeContainer.isotope({
				filter: defaultSelection,
				itemSelector: '.isotope-item'
			});


			$('.isotope-filters span').on('click', function () {
				$('.isotope-filters .current').removeClass('current');
				$(this).addClass('current');

				var selector = $(this).attr('data-filter');
					isotopeContainer.isotope({
						filter: selector,
					});
				return false;
			});

			setTimeout(function () {
				sidebarWrapper.isotope({
					itemSelector: '.isotope-item'
				});
			}, 1500);

		},
	};

	$(document).ready(function(){
		TeslaThemes.init();

		setTimeout(function () {
			jQuery('body').addClass('dom-ready');
		}, 200);
	});
}(jQuery));
