<?php
// define variables and set to empty values
$nameErr = $emailErr = $genderErr = $websiteErr = "";
$name = $email = $meassage = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
   if (empty($_POST["name"])) {
     $nameErr = "Name is required";
   } else {
     $name = test_input($_POST["name"]);
     // check if name only contains letters and whitespace
     if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
       $nameErr = "Only letters and white space allowed";
     }
   }

   if (empty($_POST["email"])) {
     $emailErr = "Email is required";
   } else {
     $email = test_input($_POST["email"]);
     // check if e-mail address is well-formed
     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
       $emailErr = "Invalid email format";
     }
   }

   if (empty($_POST["message"])) {
     $comment = "";
   } else {
     $comment = test_input($_POST["message"]);
   }
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

$to = "reachanubhavi@gmail.com";
$subject = 'Mail Form Anubhavi';
$message = "Name : " .$name ."\r\n" ."Email: " .$email ."\r\n" .$comment;

$headers = array("From: reachanubhavi@gmail.com", "Reply-To: reachanubhavi@gmail.com", "X-Mailer: PHP/".PHP_VERSION);
$headers = implode("\r\n", $headers);

mail($to, $subject, $message, $headers);
?>
OK
